export VERSION=$(shell python3 setup.py version|tail -n1)
export ARCH=$(shell uname -m)
export SDIST_GZ="dist/vula_libnss-${VERSION}.tar.gz"

.PHONY: test wheel pypi-upload deb apt-install-deps deb-in-podman clean

wheel:
	/usr/bin/python3 -m build

pypi-upload:
	# The following is because pypi blocks things unless they are built with a Docker SPOF
	# so we pretend to be manylinux but only expect to support current Debian and Ubuntu systems
	mv dist/vula_libnss-$(VERSION)-*.whl \
		dist/vula_libnss-$(VERSION)-cp38-cp38-manylinux2014_$(ARCH).whl
	python3 -m twine upload --repository pypi dist/*$(VERSION)*

deb: wheel
	echo "Building ${VERSION}"
	cd dist && tar -xvzf ../${SDIST_GZ}
	cp -arv debian dist/vula_libnss-$(VERSION)/
	cd dist/vula_libnss-$(VERSION) && DEB_BUILD_OPTIONS=nocheck CC=${CC} dpkg-buildpackage --build=binary -rfakeroot -uc

install-deps:
	apt install -y --no-install-recommends build-essential ca-certificates \
	coreutils dh-python fakeroot gcc git python3 python3-all-dev debhelper \
	python3-build python3-pip python3-setuptools python3-venv python3-wheel


deb-and-wheel-in-podman:
	echo "Building ${VERSION}"
	podman run -v `pwd`:/vula_libnss --workdir /vula_libnss --rm -it debian:bookworm bash -c 'apt update && apt install -y --no-install-recommends make && make install-deps && make wheel && make deb && make version'
	echo "Built ${VERSION} for ${ARCH}"

version:
	echo "${VERSION}"

clean:
	-rm -rf build/ dist/ vula_libnss.egg-info deb_dist
	-cd nss-altfiles && make clean
	-rm -rf vula_libnss-*.tar.gz


